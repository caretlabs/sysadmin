#!/bin/sh

#--------------------------------------------------------------------------------------
# Initialise server based on Digital Ocean's Guide for Ubuntu 14.04
# https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04
# 

## ---- Configuration 
NEWUSR="master"
INSTALL=nano screen git sudo wget curl ntp htop

#Default Password is set to the current month. Update after login.
TMPPWD=`date "+%B%Y"`

#APTINSTALL="nano screen git sudo wget curl ntp"
#SSHKEYURL="https://raw.githubusercontent.com/CaretLab/publickeys/master/d-caretlabs"





# Check-if-root Init
FILE="/tmp/out.$$"
GREP="/bin/grep"
#....

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

##--- Ok, we are root. Lets go!

#Verify
echo "New User : $NEWUSR"
sleep 3
# End Verify


#Install Stuff
apt-get -q update
apt-get upgrade -y
apt-get install -y $INSTALL

echo "$TMPPWD" | passwd --stdin root
echo "$TMPPWD" > /root/PLS_CHANGE_PASSWORD.txt

#Create New SUDO user
adduser --quiet --gecos " " $NEWUSR
echo "$TMPPWD" | passwd --stdin $NEWUSR
gpasswd -a $NEWUSR sudo

su - "$NEWUSR"
mkdir -p ".ssh"
curl https://raw.githubusercontent.com/CaretLab/publickeys/master/d-caretlabs > ".ssh/authorized_keys"
chmod 600 ".ssh/authorized_keys"

exit

#Deny Root SSH Login
sed -i 's/^#?PermitRootLogin .*/PermitRootLogin no/' /etc/ssh/sshd_config
service ssh restart


#Create Swap
#fallocate -l 2G /swapfile
dd if=/dev/zero of=/swapfile count=2048 bs=1M
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
sh -c 'echo "/swapfile none swap sw 0 0" >> /etc/fstab'
