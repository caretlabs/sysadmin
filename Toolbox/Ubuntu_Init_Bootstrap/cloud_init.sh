#!/bin/sh
################################################################################
# CloudInit bash script hook for Caret Labs servers.
# Configured to hook to internal configuration files.
# by Daniel Ting, 9th October 2015
################################################################################

INSTALL="curl wget git nano screen";
RUN=https://gitlab.com/caretlabs/sysadmin/raw/master/Toolbox/Ubuntu_Init_Bootstrap/ubuntu_setup.sh

## Become Root
su - root

    # Make sure only root can run our script
    if [[ $EUID -ne 0 ]]; then
       echo "This script must be run as root" 1>&2
       exit 1
    fi

## Setup Dependencies
apt-get update && apt-get upgrade -y;
apt-get install -y $INSTALL;


## Retrieve and run hooks.
wget -q "$RUN" -O- | bash;
exit;
