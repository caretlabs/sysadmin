#!/bin/bash
# As a user, retrieve a list of keys to add to the user's
# authorized_keys list.

# CONFIGURE
AUTH_SRC=
AUTH_KEYS_PATH=
AUTH_PINGBACK_URI=

# RUN
echo "" > $AUTH_KEYS_PATH;
wget $AUTH_SRC -O- | xargs wget -O- >> $AUTH_KEYS_PATH;
curl "$AUTH_PINGBACK_URI/?src=$HOSTNAME&usr=$USER";
